﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player2Controls : MonoBehaviour
{
    public Camera cam;
    public int playernum = 2;

    private float speedLimit = 10;
    private float jumpHeight = 350;
    private float forceAmount = 10;

    private bool jump = false;

    // Set rigid body for plyaer 2
    private Rigidbody player2;

    void Start()
    {
        switch (playernum)
        {
            case 1:
                cam.rect = new Rect(0, 0, 1, 0.5f);
                break;
            case 2:
                cam.rect = new Rect(0, 0.5f, 1, 0.5f);
                break;
            default:
                Debug.Log("Invalid player number. " + playernum);
                break;
        }

        player2 = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (Input.GetButtonDown("Jump Player2"))
        {
            jump = true;
        }
    }

    // Set player movement for velocity
    void FixedUpdate()
    {
        float horiz = Input.GetAxis("Horizontal Player2");

        if (player2.velocity.magnitude < speedLimit)
        {
            Debug.Log("Horizontal movement");
            player2.AddForce(forceAmount * new Vector3(horiz, 0, 0));
        }

        // use force to make player jump
        if (jump)
        {
            Debug.Log("Player Jumps");
            player2.velocity = new Vector3(0, player2.velocity.x, 0);
            player2.AddForce(Vector3.up * jumpHeight);
            jump = false;
        }

    }
}
