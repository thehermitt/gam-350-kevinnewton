﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Win : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(CompareTag("Player1"))
        {
            SceneManager.LoadScene(2);
        }

        if(CompareTag("Player2"))
        {
            SceneManager.LoadScene(3);
        }
    }
}
